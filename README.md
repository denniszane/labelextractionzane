# README #


- All .py files have header comments. To run the algorithm and get basic feedback with output of all labels, run main.py. To run the algorithm on only the entries in standards, use mainV2.py. To perform more analysis on the output, AFTER running main.py run secondaryEval.py.

- metrics.csv contains the Precision, Recall, and F-Measure values (in comma-separated value form) for Zane's label extraction algorithm, when tested against the wrenlab Gold Standard.

- yHat.tsv contains the labels extracted by the algorithm, indexed by GEO SampleID (in tab-separated value form)