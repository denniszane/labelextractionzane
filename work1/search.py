"""
    search.py

    -This file contains functions to be used by the methods in identification.py to search individual strings for answers, as well as a few other supporting functions and functions for interfacing with the BTO.

    -Yes, portions of this code can and should be reformatted to make use of loops for the sake of readability. Meant to get around to that but ran out of time. Sorry about that. Functionality should be fine, though.

    -NOTE: This file does have a MAIN in it (I'm not sure if that's technically the right term) between a few of the functions. These sections of code initialize the BTO OntologyZ object, as well as a dictionary containing specificity levels of BTO entries and a networkX graph of the BTO. 
"""

from .ontologyZ import *
from random import randint


def SearchGender(st):
    stLower = st.lower()
    if stLower.find("gender") != -1:
        stFound = stLower[stLower.find("gender") + len("gender"):]
        while stFound and not stFound[0].isalpha():
            stFound = stFound[1:]
        if(stFound):
            if(stFound[0].upper() == "M" or stFound[0].upper() == "F"):
                return stFound[0].upper()
    elif stLower.find("sex") != -1:
        stFound = stLower[stLower.find("sex") + len("sex"):]
        while stFound and not stFound[0].isalpha():
            stFound = stFound[1:]
        if(stFound):
            if(stFound[0].upper() == "M" or stFound[0].upper() == "F"):
                return stFound[0].upper()

def SearchGST(st):
    femaleGSTs = ["female", "woman", "girl", "ovar", "mammary", "uter", "menstru", "placenta"]
    maleGSTs = ["prostate", "testes", "testic", "foreskin", "male", "boy", " man "]

    stLower = st.lower()
    for term in femaleGSTs:
        if stLower.find(term) != -1:
            return "F"
    for term in maleGSTs:
        if stLower.find(term) != -1:
            return "M"



def SearchAge(st):
    digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    stLower = st.lower()
    if stLower.find("age") != -1 and not stLower[stLower.find("age")-1].isalpha(): #and len(stLower) > stLower.find("age")+len("age") and not stLower[stLower.find("age") + len("age")].isalpha():
        #print(stLower)
        stFound = stLower[stLower.find("age") + len("age"):]
        while stFound and not stFound[0] in digits:
            #print(stFound)
            stFound = stFound[1:]
        if(stFound):
            age = stFound[0]
            while len(stFound) > 1 and (stFound[1] in digits or stFound[1] == "."):
                age += stFound[1]
                stFound = stFound[1:]
            return age
    elif stLower.find(" age") != -1:
        stFound = stLower[stLower.find(" age") + len(" age"):]
        while stFound and not stFound[0] in digits:
            stFound = stFound[1:]
        if(stFound):
            age = stFound[0]
            while len(stFound) > 1 and stFound[1] in digits:
                age += stFound[1]
                stFound = stFound[1:]
            return age
    elif stLower.find("\tage") != -1:
        stFound = stLower[stLower.find("\tage") + len("\tage"):]
        while stFound and not stFound[0] in digits:
            stFound = stFound[1:]
        if(stFound):
            age = stFound[0]
            while len(stFound) > 1 and stFound[1] in digits:
                age += stFound[1]
                stFound = stFound[1:]
            return age
    elif stLower.find("\nage") != -1:
        stFound = stLower[stLower.find("\nage") + len("\nage"):]
        while stFound and not stFound[0] in digits:
            stFound = stFound[1:]
        if(stFound):
            age = stFound[0]
            while len(stFound) > 1 and stFound[1] in digits:
                age += stFound[1]
                stFound = stFound[1:]
            return age
    elif stLower.find("age:") != -1:
        stFound = stLower[stLower.find("age:") + len("age:"):]
        while stFound and not stFound[0] in digits:
            stFound = stFound[1:]
        if(stFound):
            age = stFound[0]
            while len(stFound) > 1 and stFound[1] in digits:
                age += stFound[1]
                stFound = stFound[1:]
            return age


def SearchAge2(st):
    digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    stLower = st.lower()
    if stLower.find("year") > 0:
        stStripped = "".join(stLower.split())
        i = stStripped.find("year")
        while i >= 0 and stStripped[i] not in digits:
            i -= 1
        while stStripped[i-1] in digits:
            i -= 1
        age = stStripped[i]
        i += 1
        while stStripped[i] in digits:
            age += stStripped[i]
            i += 1
        if age:
            return age
    elif stLower.find("month") > 0:
        stStripped = "".join(stLower.split())
        i = stStripped.find("month")
        while i > 0 and stStripped[i] not in digits:
            i -= 1
        while stStripped[i-1] in digits: 
            i -= 1
        age = stStripped[i]
        i += 1
        while stStripped[i] in digits:
            age += stStripped[i]
            i += 1
        if age:
            try:
                return float(age)/12.0
            except:
                return
    elif stLower.find("yo") > 0 and not stLower[stLower.find("yo")-1].isalpha():
        if len(stLower) > stLower.find("yo") + 2:
            if(stLower[stLower.find("yo") + 2]):
                stStripped = "".join(stLower.split())
                i = stStripped.find("yo")
                while i >= 0 and stStripped[i] not in digits:
                    i -= 1
                while stStripped[i-1] in digits:
                    i -= 1
                age = stStripped[i]
                i += 1
                while stStripped[i] in digits:
                    age += stStripped[i]
                    i += 1
                if age:
                    return age




BTO = fetchZ("BTO")
"""print(BTO)
print("TERMS:")
print(BTO.terms)
print()
print("SYNONYMS:")
print(BTO.synonyms)
print()
print("RELATIONS:")
print(BTO.relations)
print()"""

ontologyLevels = {}


def Level(bi):
    global ontologyLevels
    if bi not in ontologyLevels:
        ontologyLevels[bi] = 1
    else:
        return ontologyLevels[bi]
    if bi != "BTO:0000000":
        #print(bi)
        ixes = BTO.relations[BTO.relations["Agent"] == bi].index.tolist()
        if ixes:
            ontologyLevels[bi] = 1 + min(Level(x) for x in BTO.relations.iloc[ixes].Target)
    return ontologyLevels[bi]


for bi in BTO.terms.index:
    Level(bi)

olDf = pd.DataFrame.from_dict(data=ontologyLevels, orient='index')
olDf.columns = ["Level"]
olDf.sort_index(inplace=True)
olDf.to_csv("levels.tsv", sep="\t")



def SearchTissue(st):
    #global BTO
    #if BTO is None:
        #BTO = fetch("BTO")
        #print(BTO)
    matches = BTO.MatchImproved(st)
    confidence = {}
    if matches:
        for m in matches:
            if m in confidence:
                confidence[m] += 1
            else:
                confidence[m] = 1
    #print(matches)
    #print(confidence)
    if 368 in confidence:
        del confidence[368]
    if 4304 in confidence:
        del confidence[4304]
    if 3366 in confidence:
        del confidence[3366]
    if 1239 in confidence:
        del confidence[1239]
    if 1300 in confidence:
        del confidence[1300]
    if 2168 in confidence:
        del confidence[2168]
    return confidence

def SearchCellLine(st):
    primaryTriggers = ["cell line:", "cell_line:", "cl_name:", "cell type:", "cells:"]
    delimiters = [",", ";", "\t", "\n", "(", ")"]
    filt = [368, 4304, 3366, 1239, 1300, 2168]

    stLower = st.lower()
    isLine = False
    if "line" in stLower:
        isLine = True

    totalMatches = []
    for trigger in primaryTriggers:
        if stLower.find(trigger) != -1:
            stFound = stLower[stLower.find(trigger) + len(trigger):]
            while stFound and not stFound[0].isalpha():
                stFound = stFound[1:]
            i = 0
            while i < len(stFound) and stFound[i] not in delimiters:
                i += 1
            stFound = stFound[:i]
            if stFound:
                perms = Permutations(stFound, isLine)
                #print(stFound)
                if perms:
                    for p in perms:
                        #matches = BTO.MatchV2(p)
                        matches = BTO.MatchImproved(p)
                        for f in filt:
                            if f in matches:
                                matches.remove(f)
                        #print(matches)
                        if matches:
                            totalMatches += matches
    
    if totalMatches:
        return GetMostSpecific(totalMatches)





def Permutations(st, isLine):
    stSplits = st.split()
    if stSplits:
        for w in ["cell", "line", "cells"]:
            if w in stSplits:
                stSplits.remove(w)
    
    if stSplits:
        l = len(stSplits)
        perms = []
    
        for c in range(0,l):
            p = [c]
            e = c+1
            while e < l:
                word = ""
                for i in p:
                    word += (stSplits[i] + " ")
                #word = word[:len(word)-1]
                perms.append(word[:len(word)-1])
                perms.append(word + "cell")
                perms.append(word + "cells")
                if isLine:
                    perms.append(word + "cell line")
                e = p[len(p)-1] + 1
                p.append(e)
    
        word = stSplits[len(stSplits)-1]
        perms.append(word)
        perms.append(word + " cell")
        perms.append(word + " cells")
        if isLine:
            perms.append(word + " cell line")

        return perms


def GetName(bi):
    return BTO.GetName(bi)

def FloatToBTO(m):
    bi = str(int(m))
    zeroes = ""
    for i in range(0, 7 - len(bi)):
        zeroes += "0"
    return "BTO:" + zeroes + bi

def GetMostSpecific(matches):
    matchLevels = {}
    for m in matches:
        bi = str(int(m))
        zeroes = ""
        for i in range(0,7-len(bi)):
            zeroes += "0"
        matchLevels[m] = ontologyLevels["BTO:" + zeroes + bi]
    return max(matchLevels, key=matchLevels.get)


btoGraph = BTO.to_graph(relations=["is_a", "develops_from", "part_of", "related_to"])
btoGraph = btoGraph.to_undirected()


def GetDistance(predicted, correct):
    pi = FloatToBTO(predicted)
    ci = FloatToBTO(correct)
    return len(nx.shortest_path(btoGraph, pi, ci)) - 1

