"""
    secondaryEval.py

    -This file contains code that when run AFTER main.py (but not necessarily mainV2.py) will give various useful information about the data.

    -IMPORTANT NOTE: just like with mainV2.py, in order to run this it is highly recommended to first comment out the MAIN section of main.py (denoted by the "MAIN starts here" comment) so that it doesn't run the entirety of main.py first, because that is unnecessary if you've already run it. Unless you just want to skip to just running this one, which I guess would work as well.
"""

import pandas as pd
import numpy as np
from .search import *
from .main import GoldStandard, XiavanStandard







#MAIN starts here

yHat = (pd.read_csv("/home/dennisz/Zane/yHat.tsv", sep="\t")).set_index(["SampleID"])
w = GoldStandard("wlCorrected.tsv", True)
x = XiavanStandard()
z = GoldStandard("zaneStandard.csv", False)
m = GoldStandard("mouseStandard.csv", False)

print("TOTAL SAMPLES PROCESSED:")
print(len(yHat.index))
print()
print()


tissueFreq = {}
for bi in range(0, 5812): #BTO goes to 5811
    val = (yHat.TissueID == bi).sum()
    if val != 0:
        tissueFreq[bi] = val
        #print(bi, ": ", val)

print("TISSUE FREQUENCY:")
print()
nextFreq = tissueFreq
for i in range(1, 51):
    highest = max(nextFreq, key=nextFreq.get)
    print(i, ". ", GetName(highest), "(" ,highest, "): ", nextFreq[highest])
    del nextFreq[highest]


print()
print()
print()
print("AGE FREQUENCY:")
print("<1:", ((yHat.Age <= 12.0)).sum())
print("1 - 10:", ((yHat.Age > 12.0) & (yHat.Age <= 120.0)).sum())
print("11 - 20:", ((yHat.Age > 120.0) & (yHat.Age <= 240.0)).sum())
print("21 - 30:", ((yHat.Age > 240.0) & (yHat.Age <= 360.0)).sum())
print("31 - 40:", ((yHat.Age > 360.0) & (yHat.Age <= 480.0)).sum())
print("41 - 50:", ((yHat.Age > 480.0) & (yHat.Age <= 600.0)).sum())
print("51 - 60:", ((yHat.Age > 600.0) & (yHat.Age <= 720.0)).sum())
print("61 - 70:", ((yHat.Age > 720.0) & (yHat.Age <= 840.0)).sum())
print("71 - 80:", ((yHat.Age > 840.0) & (yHat.Age <= 960.0)).sum())
print("81 - 90:", ((yHat.Age > 960.0) & (yHat.Age <= 1080.0)).sum())
print("91+:", ((yHat.Age > 1080.0) & (yHat.Age <= 1440.0)).sum())


print()
print()
print()
print("GENDER DISTRIBUTION:")
print()
print("M:", (yHat.Gender == "M").sum())
print("F:", (yHat.Gender == "F").sum())


print()
print()
print()
print()
print("RESPONSE ANALYSIS:")
print()

wErrors = pd.read_csv("/home/dennisz/Zane/wErrors.tsv", sep="\t")
pErrors = pd.read_csv("/home/dennisz/Zane/xErrors.tsv", sep="\t")
zErrors = pd.read_csv("/home/dennisz/Zane/zErrors.tsv", sep="\t")
mErrors = pd.read_csv("/home/dennisz/Zane/mErrors.tsv", sep="\t")

print()
print("PELORUS STANDARD:")
print()

#print(pErrors)
pids = pErrors.index.values
distanceCounts = {}
fnCount = 0
for i in pids:
    #print(i, ":")
    #print(GetDistance(pErrors.ix[i].TP, pErrors.ix[i].TC))
    predicted = pErrors.ix[i].TP
    correct = pErrors.ix[i].TC
    if predicted == predicted and correct == correct:
        #print(GetDistance(predicted, correct))
        distance = GetDistance(predicted, correct)
        if distance not in distanceCounts:
            distanceCounts[distance] = 1
        else:
            distanceCounts[distance] += 1
    elif correct == correct:
        #print("False negative")
        fnCount += 1

print(distanceCounts)
print("Incorrect Answers:", sum(distanceCounts.values()))
print("False Negatives:", fnCount)
print("True Negatives:" , x.TissueID.isnull().sum())

print()
print()
print("ZANE STANDARD:")
#print(zErrors)

zids = zErrors.index.values
distanceCounts = {}
fnCount = 0
for i in zids:
    #print(i, ":")
    #print(GetDistance(pErrors.ix[i].TP, pErrors.ix[i].TC))
    predicted = zErrors.ix[i].TP
    correct = zErrors.ix[i].TC
    if predicted == predicted and correct == correct:
        #print(GetDistance(predicted, correct))
        distance = GetDistance(predicted, correct)
        if distance not in distanceCounts:
            distanceCounts[distance] = 1
        else:
            distanceCounts[distance] += 1
    elif correct == correct:
        #print("False negative")
        fnCount += 1

print(distanceCounts)
print("Incorrect Answers:", sum(distanceCounts.values()))
print("False Negatives:", fnCount)


print()
print()
print("GOLD STANDARD:")

wids = wErrors.index.values
distanceCounts = {}
fnCount = 0
for i in wids:
    predicted = wErrors.ix[i].TP
    correct = wErrors.ix[i].TC
    if predicted == predicted and correct == correct:
        distance = GetDistance(predicted, correct)
        if distance not in distanceCounts:
            distanceCounts[distance] = 1
        else:
            distanceCounts[distance] += 1
    elif correct == correct:
        fnCount += 1

print(distanceCounts)
print("Incorrect Answers:", sum(distanceCounts.values()))
print("False Negatives:", fnCount)


print()
print()
print("MOUSE STANDARD:")

mids = mErrors.index.values
distanceCounts = {}
fnCount = 0
for i in mids:
    predicted = mErrors.ix[i].TP
    correct = mErrors.ix[i].TC
    if predicted == predicted and correct == correct:
        distance = GetDistance(predicted, correct)
        if distance not in distanceCounts:
            distanceCounts[distance] = 1
        else:
            distanceCounts[distance] += 1
    elif correct == correct:
        fnCount += 1

print(distanceCounts)
print("Incorrect Answers:", sum(distanceCounts.values()))
print("False Negatives:", fnCount)
