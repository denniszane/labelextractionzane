"""
dataCollection.py

    -This file contains a pair of functions to access the GEO database and return it as a pandas DataFrame.
"""

import sqlite3

import wrenlab.ncbi.taxonomy
from pandas import DataFrame, read_csv
import pandas as pd
import wrenlab.util

from label_extraction.util import memoize



def Connect():
    url = "https://gbnci-abcc.ncifcrf.gov/geo/GEOmetadb.sqlite.gz"
    print ("Calling Connect function")
    path = str(wrenlab.util.download(url, decompress = True))
    return sqlite3.connect(path)

@memoize
def CollectSampleText():
    connection = Connect()

    query = """
            SELECT
                CAST(SUBSTR(gsm, 4) AS INT) AS 'SampleID',
                CAST(SUBSTR(gpl, 4) AS INT) AS 'PlatformID',
                title as Title,
                description as Description,
                characteristics_ch1 as Characteristics,
                source_name_ch1 as SourceName
            FROM gsm
            WHERE
                channel_count == 1
            """

    df = pd.read_sql(query, connection)
    return df.set_index(["SampleID"])




