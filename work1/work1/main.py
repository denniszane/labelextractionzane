"""
    main.py

    -This file is the primary MAIN of the label extraction algorithm. It performs the entire process from data collection to evaluation.

    -Included functions are also used in mainV2.py and secondaryEval.py
"""

import os.path

import pandas as pd
import numpy as np
import sklearn.metrics

from wrenlab.util import align_series
from .dataCollection import CollectSampleText
from .search import *
from .identification import *




def asFloat(x):
    try:
        return float(x)
    except:
        return np.nan

def GoldStandard(fileName, original):
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), fileName))
    if original:
        gs = pd.read_csv(path, sep="\t", encoding="iso-8859-1").drop_duplicates(["GSMID"])
        #print(gs)
        #print()
        #print("Column names: ", gs.columns)
    else:
        gs = pd.read_csv(path)
        #gs.columns = ["GSMID", "BTOID", "age", "gender"]
        #print(gs)
        #print()
        #print("Column names: ", gs.columns)

    gs["TissueID"] = [int(x[4:]) if (x == x) else None for x in gs.BTOID]
    gs["SampleID"] = gs.GSMID.apply(lambda x: int (x[3:]))
    gs["Gender"] = [x[0].upper() if (x == x) else None for x in gs.gender]
    gs["Age"] = gs.age.apply(asFloat).astype(float) * 12
    gs = gs.loc[:, ["SampleID", "Gender", "Age", "TissueID"]]
    return gs.set_index(["SampleID"])

def XiavanStandard():
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "pelorus_annotationsCorrected.tsv"))
    xs = pd.read_csv(path, sep="\t")
    
    xsRecArray = xs.loc[:, ["annotation", "annotation_type", "gsm"]].to_records()
    #print(xsRecArray)

    data = {}
    
    for i, anno, annoType, gsm in xsRecArray:
        if annoType != "disease" and anno and (anno == anno):
            #print(gsm, ": ", annoType, " = ", anno)
            #print()
            #gsm = int(gsm)
            if gsm in data:
                data[gsm][annoType] = anno
            else:
                data[gsm] = {annoType : anno}
    
    xsFinal = pd.DataFrame.from_dict(data=data, orient='index')
    #print()
    #print("IMMEDIATELY AFTER CREATION:")
    #print(xsFinal)

    xsFinal["SampleID"] = xsFinal.index
    xsFinal["SampleID"] = xsFinal.SampleID.apply(lambda x: int (x[3:]))
    xsFinal["Gender"] = [x[0].upper() if (x and (x==x)) else None for x in xsFinal.gender]
    xsFinal["Age"] = xsFinal.age.apply(asFloat).astype(float)
    xsFinal["TissueID"] = [float(x[4:]) if ((x==x) and len(x) == 11) else None for x in xsFinal.origin]
    #print()
    #print("AFTER NORMALIZATION:")
    #print(xsFinal)

    #xsFinal = xsFinal.where((pd.notnull(xsFinal)), None)

    xsFinal = xsFinal.loc[:, ["SampleID", "Gender", "TissueID", "Age"]]
    xsFinal = xsFinal.set_index(["SampleID"])
    return xsFinal.sort_index()
    


def EvaluateGender(y, yHat):
    print("\n######### GENDER #########\n")
    y2, yHat2 = align_series(y.Gender.dropna(), yHat.Gender, dropna=False)
    returnValues = []
    for gender in ["M", "F"]:
        print("Gender = ", gender)
        truePos = ((y2 == gender) & (yHat2 == gender)).sum()
        incAns = ((y2 == gender) & ~yHat2.isnull() & (yHat2 != gender)).sum()
        falsePos = ((y2.isnull() & ~yHat2.isnull())).sum()
        falseNeg = yHat2.isnull().sum()

        print()
        print(" -True Positives:", truePos)
        print(" -Incorrect Answers:", incAns)
        print(" -False Positives:", falsePos)
        print(" -False Negatives:", falseNeg)
        print()
        
        precision = round(truePos / (truePos + incAns), 4)
        recall = round(truePos / (truePos + falseNeg + incAns), 4)

        print(" -N: ", (y2 == gender).sum())
        print(" -PRECISION: ", precision)
        print(" -RECALL: ", recall)
        fMeasure = round(2 * (precision * recall) / (precision + recall), 4)
        print(" -F-MEASURE: ", fMeasure)
        returnValues.append(precision)
        returnValues.append(recall)
        returnValues.append(fMeasure)
        
    return returnValues



def EvaluateAge(y, yHat):
    print("\n########## AGE ##########\n")
    tolerance = 0.5
    
    y2, yHat2 = align_series(y.Age.dropna(), yHat.Age, dropna=False)
    dx = (y2 - yHat2).abs()
    print("N:", y2.shape[0])
    print("MAD: ", round(dx.mean(),2), "[months]")
    truePos = (dx < tolerance).sum()
    incAns = (dx >= tolerance).sum()
    falsePos = ((y2.isnull()) & ~yHat2.isnull()).sum()
    falseNeg = yHat2.isnull().sum()
    
    print()
    print("True Positives:", truePos)
    print("Incorrect Answers:", incAns)
    print("False Positives:", falsePos)
    print("False Negatives:", falseNeg)
    print()
    
    precision = round(truePos / (truePos + incAns), 4)
    recall = round(truePos / (truePos + falseNeg + incAns), 4)
    print("PRECISION: " , precision)
    print("RECALL: ", recall)
    fMeasure = round(2 * (precision * recall) / (precision + recall), 4)
    print("F-MEASURE: ", fMeasure)

    return precision, recall, fMeasure



def EvaluateTissue(y, yHat):
    print("\n########## TISSUE ID ##########\n")
    y2, yHat2 = align_series(y.TissueID.dropna(), yHat.TissueID, dropna=False)

    print("N: ", y2.shape[0])
    truePos = (y2 == yHat2).sum()
    incAns = ((y2 != yHat2) & ~yHat2.isnull()).sum()
    falsePos = ((y2.isnull()) & ~(yHat2.isnull())).sum()
    falseNeg = yHat2.isnull().sum()
     
    print()
    print("True Positives:", truePos)
    print("Incorrect Answers:", incAns)
    print("False Positives:", falsePos)
    print("False Negatives:", falseNeg)
    print()

    precision = round(truePos / (truePos + incAns), 4)
    recall = round(truePos / (truePos + falseNeg + incAns), 4)
    print("M-A PRECISION: ", precision)
    print("M-A RECALL: ", recall)
    fMeasure = round(2 * (precision * recall) / (precision + recall), 4)
    print("F-MEASURE: ", fMeasure)

    return precision, recall, fMeasure


def Errors(y, yHat, outputFile):
    print()
    print()
    print()
    print("Identifying errors...")
    print()
    ids = y.index.values
    errorIDs = []
    genderErrors = []
    genderAnswers = []
    ageErrors = []
    ageAnswers = []
    tissueErrors = []
    tissueAnswers = []
    for sampleid in ids:
        #print("Correct: ", y.ix[sampleid])
        if sampleid in yHat.index:
            if(y.ix[sampleid].Gender != yHat.ix[sampleid].Gender):
                #print(sampleid, ": ", yHat.ix[sampleid].Gender)
                #print("Correct: ", y.ix[sampleid].Gender)
                #print()
                errorIDs.append(sampleid)
                genderErrors.append(yHat.ix[sampleid].Gender)
                genderAnswers.append(y.ix[sampleid].Gender)
                ageErrors.append(None)
                ageAnswers.append(None)
                tissueErrors.append(None)
                tissueAnswers.append(None)
            if abs(y.ix[sampleid].Age - yHat.ix[sampleid].Age) > 0.5:
                #print(sampleid, ": ", yHat.ix[sampleid].Age)
                #print("Correct: ", y.ix[sampleid].Age)
                #print()
                errorIDs.append(sampleid)
                genderErrors.append(None)
                genderAnswers.append(None)
                ageErrors.append(yHat.ix[sampleid].Age)
                ageAnswers.append(y.ix[sampleid].Age)
                tissueErrors.append(None)
                tissueAnswers.append(None)
            if y.ix[sampleid].TissueID != yHat.ix[sampleid].TissueID and y.ix[sampleid].TissueID == y.ix[sampleid].TissueID:
                pass
                #print(sampleid, ": ", yHat.ix[sampleid].TissueID)
                #print("Correct: ", y.ix[sampleid].TissueID)
                #print()
                errorIDs.append(sampleid)
                genderErrors.append(None)
                genderAnswers.append(None)
                ageErrors.append(None)
                ageAnswers.append(None)
                tissueErrors.append(yHat.ix[sampleid].TissueID)
                tissueAnswers.append(y.ix[sampleid].TissueID)
    
    errorsDataSet = list(zip(errorIDs, genderErrors, genderAnswers, ageErrors, ageAnswers, tissueErrors, tissueAnswers))
    #print(errorsDataSet)
    errors = pd.DataFrame(data=errorsDataSet, columns=["ID", "GP", "GC", "AP", "AC", "TP", "TC"])
    errors = errors.sort_values(by=["ID"])
    errors = errors.set_index(["ID"])
    errors.to_csv(outputFile, sep="\t")
    return errors

def ConfusionMatrix(errors, colNames):
    errorsDict = {}
    for n in colNames:
        errorsDict[str(n)] = {}

    correct = errors.TC.values
    predicted = errors.TP.values
    predicted = [int(i) if(i==i) else None for i in predicted]
    #print("Correct: ", correct)
    #print("Predicted: ", predicted)
    #print()

    for i in range(0, len(correct)):
        if correct[i]==correct[i] and predicted[i]==predicted[i]:
            if correct[i] in errorsDict and predicted[i] in errorsDict[correct[i]]:
                #print("adding to dict for key=",correct[i])
                errorsDict[correct[i]][predicted[i]] += 1
            elif correct[i] in errorsDict:
                #print("adding to dict for key=",correct[i])
                errorsDict[correct[i]].update({predicted[i] : 1})
            else:
                #print("establishing new subdict for key=",correct[i])
                errorsDict[correct[i]] = {predicted[i] : 1}
    #print(errorsDict)
    cm = pd.DataFrame.transpose(pd.DataFrame.from_dict(data=errorsDict, orient='index'))
    #print("BEFORE TOTAL:")
    #print(cm)
    #print()
    #print("TOTAL:")
    total = {"Total" : {}}
    for n in colNames:
        if n == n:
            #print("N: ", n)
            #print("Sum: ", (cm.loc[:,n]).sum())
            #print()
            total["Total"][n] = (cm.loc[:,n]).sum()
    #print(total)
    total = pd.DataFrame.transpose(pd.DataFrame.from_dict(data=total))
    #print(total)
    
    cm = cm.append(total)
    return cm

def AnalyzeCM(cm):
    print()
    print("CONFUSION MATRIX ANALYSIS:")
    print()
    totals = cm.loc["Total",:]
    #print("Totals:")
    #print(totals)
    for i in range(0,10):
        print("Correct tissue:")
        print(totals.idxmax(), "(", totals[totals.idxmax()], ")")
        #print("Matrix column:")
        print()
        #print(cm.loc[:,[totals.idxmax()]])
        print("Most common incorrect prediction:")
        withoutTotal = cm.loc[:, totals.idxmax()]
        withoutTotal = withoutTotal.ix[:-1]
        print(withoutTotal.idxmax(), "(", cm.loc[withoutTotal.idxmax(), totals.idxmax()], ")")
        print()
        print()
        print()
        del totals[totals.idxmax()]

    #print("EAR ERRORS:")
    #print((cm.loc[368, :]).sum())
    #print()

    #print("TOTAL ERRORS:")
    #print((cm.loc["Total",:]).sum())
    #print()
    #print()

    commonErrorIx = 0
    commonErrorQ = 0

    for i in cm.index:
        #print(i)
        if cm.loc[i,:].sum() > commonErrorQ and i != "Total" and i:
            commonErrorQ = cm.loc[i,:].sum()
            commonErrorIx = i

    print("Most common incorrect prediction (overall, None excluded):")
    print(commonErrorIx, "(", commonErrorQ, ")")
    
    """print("Second most missed tissue:")
    del totals[totals.idxmax()]
    print(totals.idxmax(), "(", totals[totals.idxmax()], ")")
    print("Most commonly incorrectly predicted:", (cm.loc[:, totals.idxmax()]))
    
    print("Third most missed tissue:")
    del totals[totals.idxmax()]
    print(totals.idxmax(), "(", totals[totals.idxmax()], ")")
    print("Most commonly incorrectly predicted:", (cm.loc[:, totals.idxmax()]))"""


#MAIN Starts here

rawData = CollectSampleText()
print()
print()


recordArray = rawData.loc[:, ["Characteristics", "Description", "SourceName", "Title"]].to_records()
print()

ids = []
genders = []
ages = []
tissues = []

for sampleID, chars, desc, sN, title in recordArray:
    #print(sampleID)
    ids.append(sampleID)
    genders.append(IdentifyGender(sampleID, chars, desc, sN, title))
    ages.append(IdentifyAge(sampleID, chars, desc, sN, title))
    #print(sampleID, ": ", ages[len(ages)-1])
    tissues.append(IdentifyTissueV2(sampleID, chars, desc, sN, title))


yHatDataSet = list(zip(ids, genders, ages, tissues))
yHat = pd.DataFrame(data=yHatDataSet, columns=["SampleID", "Gender", "Age", "TissueID"])
yHat = yHat.set_index(["SampleID"])
yHat.to_csv("yHat.tsv", sep="\t")

w = GoldStandard("wlCorrected.tsv", True)
print("GOLD STANDARD")

genderMetrics = EvaluateGender(w, yHat)
pAge, rAge, fAge = EvaluateAge(w, yHat)
pTissue, rTissue, fTissue = EvaluateTissue(w, yHat)

print()
print()

maleMetrics = genderMetrics[:3]
femaleMetrics = genderMetrics[3:]
ageMetrics = [pAge, rAge, fAge]
tissueMetrics = [pTissue, rTissue, fTissue]
rowNames = ["Precision:", "Recall:", "F-Measure:"]

metricsData = list(zip(rowNames, ageMetrics, maleMetrics, femaleMetrics, tissueMetrics))
metrics = pd.DataFrame(data=metricsData, columns=["METRIC", "AGE", "MALE", "FEMALE", "TISSUE"])
metrics.set_index(["METRIC"])
print(metrics)
metrics.to_csv("metrics.csv")

wErrors = Errors(w, yHat, "wErrors.tsv")
#wCM = ConfusionMatrix(wErrors, wErrors.TC.unique())
#AnalyzeCM(wCM)

x = XiavanStandard()
print()
print()
print("XIAVAN STANDARD")
print()
x.to_csv("xsReadable.tsv", sep="\t")

genderMetrics = EvaluateGender(x, yHat)
pAge, rAge, fAge = EvaluateAge(x, yHat)
pTissue, rTissue, fTissue = EvaluateTissue(x, yHat)

xErrors = Errors(x, yHat, "xErrors.tsv")
#xCM = ConfusionMatrix(xErrors, xErrors.TC.unique())
#AnalyzeCM(xCM)



z = GoldStandard("zaneStandard.csv", False)
print()
print()
print("ZANE STANDARD")
#print(z)
print()
genderMetrics = EvaluateGender(z, yHat)
pAge, rAge, fAge = EvaluateAge(z, yHat)
pTissue, rTissue, fTissue = EvaluateTissue(z, yHat)

zErrors = Errors(z, yHat, "zErrors.tsv")
#zCM = ConfusionMatrix(zErrors, zErrors.TC.unique())
#AnalyzeCM(zCM)


m = GoldStandard("mouseStandard.csv", False)
print()
print()
print("MOUSE STANDARD")
print()
genderMetrics = EvaluateGender(m, yHat)
pAge, rAge, fAge = EvaluateAge(m, yHat)
pTissue, rTissue, fTissue = EvaluateTissue(m, yHat)

mErrors = Errors(m, yHat, "mErrors.tsv")
