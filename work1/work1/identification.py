"""
    identification.py

    -This file contains methods to identify Gender, Age, and Tissue for the input data of a single sample.

    -Yes, this code could and probably should be reformatted to make use of loops. Meant to get around to that, sorry I didn't.
"""

from .search import *

import pandas as pd
import numpy as np




def IdentifyGender(sampleID, chars, desc, sN, title):
    #Phase 0: Make copies
    charsCopy = chars
    descCopy = desc
    sNCopy = sN
    titleCopy = title

    #Phase 1: Standard search
    if chars:
       gen = SearchGender(chars)
       if gen:
           return gen
    if desc:
        gen = SearchGender(desc)
        if gen:
            return gen
    if sN:
        gen = SearchGender(sN)
        if gen:
            return gen
    if title:
        gen = SearchGender(title)
        if gen:
            return gen

    #Phase 2: Search for gender-specific terms
    if charsCopy:
        gen = SearchGST(charsCopy)
        if gen:
            return gen
    if descCopy:
        gen = SearchGST(descCopy)
        if gen:
            return gen
    if sNCopy:
        gen = SearchGST(sNCopy)
        if gen:
            return gen
    if titleCopy:
        gen = SearchGST(titleCopy)
        if gen:
            return gen


def IdentifyAge(sampleID, chars, desc, sN, title):
    #print(sampleID)
    #Phase 1
    if chars:
        age = SearchAge(chars)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan
    if desc:
        age = SearchAge(desc)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan
    if sN:
        age = SearchAge(sN)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan
    if title:
        age = SearchAge(title)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan
    #Phase 2
    if chars:
        age = SearchAge2(chars)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan
    if desc:
        age = SearchAge2(desc)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan
    if sN:
        age = SearchAge2(sN)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan
    if title:
        age = SearchAge2(title)
        if age:
            try:
                #if age < 120:
                age = float(age) * 12
                if age < 1440:
                    return age
            except:
                return np.nan


def IdentifyTissue(sampleID, chars, desc, sN, title):
    #print(sampleID)
    if sN:
        sResult = SearchTissue(sN)
        if sResult:
            return GetMostSpecific(sResult)

    if title:
        sResult = SearchTissue(title)
        if sResult:
            return GetMostSpecific(sResult)
    
    if chars:
        sResult = SearchTissue(chars)
        if sResult:
            return GetMostSpecific(sResult)
    
    if desc:
        sResult = SearchTissue(desc)
        if sResult:
            return GetMostSpecific(sResult)



def IdentifyTissueV2(sampleID, chars, desc, sN, title):
    #print(sampleID)
    if chars:
        result = SearchCellLine(chars)
        if result:
            return result

    return IdentifyTissue(sampleID, chars, desc, sN, title)

