"""
    mainV2.py

    -This file contains a second version of MAIN for label extraction. It is much faster, but only extracts labels for the samples in the standards. This version should be used when editing the extraction methods for evaluation, but not for final output.

    -Contains extraction and evaluation for all 4 current standards (Wren Lab, Pelorus--denoted here as Xiavan, Zane, and Mouse).

    -IMPORTANT: Because this file uses functions from main.py, the MAIN section of main.py (denoted by a comment saying "MAIN starts here" should be commented out when using this so that it doesn't run.
"""

import pandas as pd
import numpy as np
import sklearn.metrics

from wrenlab.util import align_series
from .dataCollection import CollectSampleText
from .search import *
from .identification import *
from .main import *



rawData = CollectSampleText()
print("Finished fetching raw data!")
print()
print()

x = XiavanStandard()
print()
print()
print("XIAVAN STANDARD:")
#print(x)
print()


xids = x.index.values
ids = []
genders = []
ages = []
tissues = []
for i in xids:
    if i in rawData.index.values:
        ids.append(i)
        genders.append(IdentifyGender(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        ages.append(IdentifyAge(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        tissues.append(IdentifyTissueV2(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))

print()
print("Done extracting labels for set X")
print()

xHatDataSet = list(zip(ids, genders, ages, tissues))
xHat = pd.DataFrame(data=xHatDataSet, columns=["SampleID", "Gender", "Age", "TissueID"])
xHat = xHat.set_index(["SampleID"])
#print(xHat)
print()
print()

genderMetrics = EvaluateGender(x, xHat)
pAge, rAge, fAge = EvaluateAge(x, xHat)
pTissue, rTissue, fTissue = EvaluateTissue(x, xHat)





z = GoldStandard("zaneStandard.csv", False)
print()
print()
print()
print()
print("ZANE STANDARD:")
print()

zids = z.index.values
ids = []
genders = []
ages = []
tissues = []
for i in zids:
    if i in rawData.index.values:
        ids.append(i)
        genders.append(IdentifyGender(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        ages.append(IdentifyAge(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        tissues.append(IdentifyTissueV2(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))

print()
print("Done extracting labels for set Z")
print()


zHatDataSet = list(zip(ids, genders, ages, tissues))
zHat = pd.DataFrame(data=zHatDataSet, columns=["SampleID", "Gender", "Age", "TissueID"])
zHat = zHat.set_index(["SampleID"])
#print(zHat)
print()
print()

genderMetrics = EvaluateGender(z, zHat)
pAge, rAge, fAge = EvaluateAge(z, zHat)
pTissue, rTissue, fTissue = EvaluateTissue(z, zHat)

zHat.to_csv("zHat.tsv", sep="\t")





w = GoldStandard("wlCorrected.tsv", True)
print()
print()
print()
print()
print("GOLD STANDARD")
print()

wids = w.index.values
ids = []
genders = []
ages = []
tissues = []
for i in wids:
    if i in rawData.index.values:
        ids.append(i)
        genders.append(IdentifyGender(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        ages.append(IdentifyAge(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        tissues.append(IdentifyTissueV2(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))

print()
print("Done extracting labels for set W")
print()

wHatDataSet = list(zip(ids, genders, ages, tissues))
wHat = pd.DataFrame(data=wHatDataSet, columns=["SampleID", "Gender", "Age", "TissueID"])
wHat = wHat.set_index(["SampleID"])

print()
print()

genderMetrics = EvaluateGender(w, wHat)
pAge, rAge, fAge = EvaluateAge(w, wHat)
pTissue, rTissue, fTissue = EvaluateTissue(w, wHat)

wHat.to_csv("wHat.tsv", sep="\t")





m = GoldStandard("mouseStandard.csv", False)
print()
print()
print()
print()
print("MOUSE STANDARD")
print()
#print(m)

mids = m.index.values
ids = []
genders = []
ages = []
tissues = []
for i in mids:
    if i in rawData.index.values:
        ids.append(i)
        genders.append(IdentifyGender(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        ages.append(IdentifyAge(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))
        tissues.append(IdentifyTissueV2(i, rawData.ix[i].Characteristics, rawData.ix[i].Description, rawData.ix[i].SourceName, rawData.ix[i].Title))

print()
print("Done extracting labels for set M")
print()

mHatDataSet = list(zip(ids, genders, ages, tissues))
mHat = pd.DataFrame(data=mHatDataSet, columns=["SampleID", "Gender", "Age", "TissueID"])
mHat = mHat.set_index(["SampleID"])

#print(mHat)
print()
print()

genderMetrics = EvaluateGender(m, mHat)
pAge, rAge, fAge = EvaluateAge(m, mHat)
pTissue, rTissue, fTissue = EvaluateTissue(m, mHat)

mHat.to_csv("mHat.tsv", sep="\t")
