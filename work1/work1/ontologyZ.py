"""
    ontologyZ.py

    -This file declares and implements the OntologyZ class, which inherits from the wrenlab Ontology class. It's pretty much the same thing but with a couple functions ever-so-slightly modified.

    -Also contains the fetchZ function, which does the same thing as the wrenlab fetch except it returns an OntologyZ rather than an Ontology.
"""

from wrenlab.ontology import *

class OntologyZ(Ontology):
    def __init__(self, terms, synonyms, relations, prefix=None):
        super(OntologyZ, self).__init__(terms, synonyms, relations, prefix=None)
    
    @property
    def trie(self):
        if not hasattr(self, "_trie"):
            o = acfsa.MixedCaseSensitivityTrie(allow_overlaps=False, boundary_characters=" .;,\t\n")
            for ix,t in self.terms.loc[:,["Name"]].to_records():
                for s in self._expand_synonym(t):
                    o.add(s, key=ix, case_sensitive=(len(s) < 5))
            for ix,t in self.synonyms.loc[:,["Term ID", "Synonym"]].to_records(index=False):
                for s in self._expand_synonym(t):
                    o.add(s, key=ix, case_sensitive=(len(s) < 5))
            o.build()
            self._trie = o
        return self._trie
    
    def MatchImproved(self, text):
        if text is None:
            return None
        trie = self.trie
        matches = trie.search(text)
        matchIDs = []
        for m in matches:
            matchIDs.append(float(m.key[4:]))
        return matchIDs

    def GetName(self, bi):
        bi = str(bi)
        ix = "BTO:"
        for i in range(0, 7-len(bi)):
            ix += "0"
        ix += bi
        if ix in self.terms.index:
            return self.terms.Name[ix]
        else:
            return "Error"


def fetchZ(name):
    onto = fetch(name) 
    
    return OntologyZ(onto.terms, onto.synonyms, onto.relations)
